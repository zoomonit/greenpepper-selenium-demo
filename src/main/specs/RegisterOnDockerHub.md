# Greenpepper Selenium sample

This test will try to register on docker hub. We will obviously fail.
So we will test that the registration is failing. 

## First import our default package 

| import |
|--------|
| com.zoomonit.greenpepper.demo |

## Pom.xml  configuration 

This test is written using markdown ([GreenPepper flavour]). So here is the configuration to support markdown

```xml
<repositories>
    <repository>
        <root>${basedir}/src/main/specs;md</root>
        <name>GreenPepper-specifications</name>
        <dialect>com.greenpepper.runner.dialect.MarkdownDialect</dialect>
    </repository>
</repositories>
```

The repository type here is the implicit default value (FileSystemRepository).
The specifications will all be located in `${basedir}/src/main/specs` and will have the `.md` extension.

I will use the `greenpepper:generate-fixtures` functionality, hence the following setting: 

```xml
<defaultPackage>com.zoomonit.greenpepper.demo</defaultPackage>
```

### Activate Selenium

As described in the [selenium extension documentation], I add the dependency for selenium:

```xml
<dependency>
    <groupId>com.github.strator-dev.greenpepper</groupId>
    <artifactId>greenpepper-extensions-selenium2</artifactId>
    <version>${greenpepper.version}</version>
</dependency>
```

and the Selenium SUD. I will select chrome as testing browser, I activate the download of the 
webdriver (version 2.35 - the one for my chrome browser version)

```xml
<systemUnderDevelopment>com.greenpepper.extensions.selenium2.SeleniumSystemUnderDevelopment</systemUnderDevelopment>
<systemUnderDevelopmentArgs>chrome;true;2.35</systemUnderDevelopmentArgs>
```
I am now ready for my test.

[GreenPepper flavour]: https://greenpepper.atlassian.net/wiki/spaces/DOC/pages/83558401/Writing+your+specification+in+MarkDown
[selenium extension documentation]: https://greenpepper.atlassian.net/wiki/spaces/DOC/pages/120160257/Selenium+extension

## The specification itself

|begin info|
|----------|

I will do the following:
  * open the browser on https://hub.docker.com/
  * fill the register form in with fake info
  * check for the error messages
  
|end info|
|--------|


|do with | Selenium |     |    |   |
|--------|----------|-----|----|---|
| accept | open the url | https://hub.docker.com/ |
| accept | click on | Sign up for Docker Hub |
| accept | fill the field | Docker ID | with | johnsmith |
| accept | fill the field | Email | with | john.smith@planet.earth |
| accept | fill the field | Password | with | newbiepassword |
| accept | click on | Sign Up |

> **Note:** The keyword `accept` is not needed at all. I just wanted some green color on my result page.

## Writing the fixture

After writing this specification, we can generate the fixtures directly into our source folder:

```bash
mvn greenpepper:generate-fixtures
```

Then, let's implement the methods. Before that, we will need to get the webDriver instance. 
We will get it by injection from the Selenium SUD:

```java
@Fixture(usage = "Selenium")
public class SeleniumFixture {

	private final WebDriver webDriver;

	@Inject
	public SeleniumFixture(WebDriver webDriver) {
		this.webDriver = webDriver;
	}
	
}
```

### Opening the URL

Using the webDriver, let's open the URL

```java
@Fixture(usage = "Selenium")
public class SeleniumFixture {
    
	@FixtureMethod(usage = "open the url")
	public void openTheUrl(String url) {
		webDriver.get(url);
	}
}
```

### Filling the form

The way the specification is written is fully understandable by anyone. 
That's because we used what was display on the website and not some technical 'id' to speak fo the inputs.
I strongly suggest keeping that rule for all your tests. They should as next as possible to the users.

Now that means that we just have the **placeholders** to use as input identifiers.

```java
@Fixture(usage = "Selenium")
public class SeleniumFixture {
    
    @FixtureMethod(usage = "fill the field with")
    public boolean fillTheFieldWith(String placeholder, String value) {
        WebElement input = webDriver.findElement(By.cssSelector(format("input[placeholder=\"%s\"]", placeholder)));
        input.sendKeys(value);
        return true;
    }
}

```


### Clicking on the button

Same as above, we search the button by its name. 

```java
@Fixture(usage = "Selenium")
public class SeleniumFixture {

	@FixtureMethod(usage = "click on")
	public boolean clickOn(String button) {
		List<WebElement> buttonsWE = webDriver.findElements(By.tagName("button"));
		for (WebElement buttonWE : buttonsWE) {
			if (StringUtils.equals(buttonWE.getText(), button)) {
				buttonWE.click();
				return true;
			}
		}
		return false;
	}
}
```

### Tips: Knowing what Selenium did.
 
 I have added the following code to highlight the found element. Note the `Thread.sleep(2000L);`

```java
@Fixture(usage = "Selenium")
public class SeleniumFixture {

	@FixtureMethod(usage = "fill the field with")
	public boolean fillTheFieldWith(String placeholder, String value) throws InterruptedException {
		WebElement input = webDriver.findElement(By.cssSelector(format("input[placeholder=\"%s\"]", placeholder)));
		highLight(input);
		input.sendKeys(value);
		Thread.sleep(2000L);
		return true;
	}

	private void highLight(WebElement element) {
		JavascriptExecutor jse = (JavascriptExecutor) webDriver;
		jse.executeScript("arguments[0].style.backgroundColor = 'yellow'", element);
	}
}
```


## The sources of this demo are available on GitLab

[https://gitlab.com/zoomonit/greenpepper-selenium-demo](https://gitlab.com/zoomonit/greenpepper-selenium-demo)

Have fun !