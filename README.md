# Greenpepper Selenium Extension usage demo

This project is a demo on how to use the Greenpepper selenium extension. 
It uses:
  * Maven
  * Greenepper Markdown Dialect.

> More info in the specifcation under [src/main/specs/RegisterOnDockerHub.md](src/main/specs/RegisterOnDockerHub.md)

## Quick testing

Here is how to quickly launch the testing 

### Prerequisites

Install JAVA 8+ JDK ( [http://jdk.java.net/](http://jdk.java.net/) ) and set your JAVA_HOME to the JDK directory.

### Launch the MVNW command

```
mvnw integration-test
```

